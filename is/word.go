package is

var Word = word{}

type word struct{}

func (word) Ident() string { return `word` }
func (word) PEGN() string  { return `upper / lower / digit / UNDER` }
func (word) Desc() string {
	return `uppercase or lowercase ASCII letter, digit 0 through 9, or underscore character _`
}
func (word) Check(r rune) bool { return Upper.Check(r) || Lower.Check(r) || Digit.Check(r) || r == '_' }
