package is

var Ws = ws{}

type ws struct{}

func (ws) Ident() string { return `ws` }
func (ws) PEGN() string  { return `SP / TAB / CR / LF` }
func (ws) Desc() string {
	return `space, tab, line feed, or carriage return`
}
func (ws) Check(r rune) bool {
	switch r {
	case ' ', '\t', '\r', '\n':
		return true
	default:
		return false
	}
}
