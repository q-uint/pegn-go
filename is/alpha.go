package is

var Alpha = alpha{}

type alpha struct{}

func (alpha) Ident() string { return "alpha" }
func (alpha) PEGN() string  { return "[A-Z] / [a-z]" }
func (alpha) Desc() string {
	return `lowercase or uppercase ASCII letter`
}
func (alpha) Check(r rune) bool {
	return Lower.Check(r) || Upper.Check(r)
}
