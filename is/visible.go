package is

var Visible = visible{}

type visible struct{}

func (visible) Ident() string     { return "visible" }
func (visible) PEGN() string      { return "alphanum / punct" }
func (visible) Desc() string      { return `visible ASCII character` }
func (visible) Check(r rune) bool { return Alphanum.Check(r) || Punct.Check(r) }
