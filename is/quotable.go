package is

/*
// Quotable returns true if rune is an alphanum, space, falls within the
// following ASCII/Unicode ranges [x20-x26], [x28-x2F], [x3A-x40],
// [x5B-x60], [x7B-x7E]. It does not include a single quote (SQ) but
// does include double quote (DQ).
func Quotable(r rune) bool {


}
*/

var Quotable = quotable{}

type quotable struct{}

func (quotable) Ident() string { return "quotable" }
func (quotable) PEGN() string {
	return "alphanum / [x20-x26] / [x28-x2F] / [x3A-x40] / [x5B-x60] / [x7B-x7E]"
}

func (quotable) Desc() string { return `visible ASCII character except single quote or a space` }
func (quotable) Check(r rune) bool {
	switch {
	case
		Alphanum.Check(r),
		r == ' ',
		'\x20' <= r && r <= '\x26',
		'\x28' <= r && r <= '\x2F',
		'\x3A' <= r && r <= '\x40',
		'\x5B' <= r && r <= '\x60',
		'\x7B' <= r && r <= '\x7E':
		return true
	default:
		return false
	}
}
