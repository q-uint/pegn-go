// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the Apache
// 2.0 license that can be found in the LICENSE file.

package is

type Min struct {
	Match interface{}
	Min   int
}

type Count struct {
	Match interface{}
	Count int
}

type Seq []interface{}

type OneOf []interface{}

type MinMax struct {
	Match interface{}
	Min   int
	Max   int
}

type Not struct {
	This interface{}
}

type Opt struct {
	This interface{}
}
