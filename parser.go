package pegn

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"unicode/utf8"

	"gitlab.com/pegn/pegn-go/is"
)

type Parser struct {
	in  io.Reader
	buf []byte
	mk  *Mark
}

func (p *Parser) bufferInput(i interface{}) error {
	var err error
	switch in := i.(type) {
	case io.Reader:
		p.buf, err = ioutil.ReadAll(in)
		if err != nil {
			return err
		}
	case string:
		p.buf = []byte(in)
	case []byte:
		p.buf = in
	default:
		return fmt.Errorf("parser: unsupported input type: %t", i)
	}
	if len(p.buf) == 0 {
		return fmt.Errorf("parser: no input")
	}
	return err
}

const pegn_ENDOFDATA = 1<<31 - 1 // max int32

func (p *Parser) Init(i interface{}) error {
	if err := p.bufferInput(i); err != nil {
		return err
	}
	cur, ln := utf8.DecodeRune(p.buf) // scan first
	if ln == 0 {
		cur = pegn_ENDOFDATA
		return fmt.Errorf("parser: failed to scan first rune")
	}
	p.mk = new(Mark)
	p.mk.Rune = cur
	p.mk.Len = ln
	p.mk.Next = ln
	p.mk.Pos.Line = 1
	p.mk.Pos.LineRune = 1
	p.mk.Pos.LineByte = 1
	p.mk.Pos.Rune = 1
	return nil
}

func (p *Parser) Next() {
	if p.Done() {
		return
	}
	rn, ln := utf8.DecodeRune(p.buf[p.mk.Next:])
	if ln != 0 {
		p.mk.Byte = p.mk.Next
		p.mk.Pos.LineByte += p.mk.Len
	} else {
		rn = pegn_ENDOFDATA
	}
	p.mk.Rune = rn
	p.mk.Pos.Rune += 1
	p.mk.Next += ln
	p.mk.Pos.LineRune += 1
	p.mk.Len = ln
}

func (p *Parser) Move(n int) {
	for i := 0; i < n; i++ {
		p.Next()
	}
}

func (p *Parser) Done() bool {
	return p.mk.Rune == pegn_ENDOFDATA && p.mk.Len == 0
}

func (p *Parser) String() string {
	if p.mk == nil {
		return "<nil>"
	}
	return p.mk.String()
}

func (p *Parser) Print() { fmt.Println(p.String()) }

func (p *Parser) Mark() *Mark {
	if p.mk == nil {
		return nil
	}
	// force a copy
	cp := *p.mk
	return &cp
}

func (p *Parser) Goto(m *Mark) { nm := *m; p.mk = &nm }

func (p *Parser) NewLine() {
	p.mk.Pos.Line++
	p.mk.Pos.LineRune = 1
	p.mk.Pos.LineByte = 1
}

func (p *Parser) Parse(m *Mark) string {
	if m.Byte < p.mk.Byte {
		return string(p.buf[m.Byte:p.mk.Next])
	}
	return string(p.buf[p.mk.Byte:m.Next])
}

func (p *Parser) Slice(beg *Mark, end *Mark) string {
	return string(p.buf[beg.Byte:end.Next])
}

func (p *Parser) Expect(ms ...interface{}) (*Mark, error) {
	var beg, end *Mark
	beg = p.Mark()
	for _, m := range ms {
		switch v := m.(type) {

		case rune:
			if p.mk.Rune != v {
				err := p.Expected(m)
				p.Goto(beg)
				return nil, err
			}
			end = p.Mark()
			p.Next()

		case string:
			if v == "" {
				return nil, fmt.Errorf("expect: cannot parse empty string")
			}
			for _, r := range []rune(v) {
				if p.mk.Rune != r {
					err := p.Expected(r)
					p.Goto(beg)
					return nil, err
				}
				end = p.Mark()
				p.Next()
			}

		case Class:
			if !v.Check(p.mk.Rune) {
				err := p.Expected(v)
				p.Goto(beg)
				return nil, err
			}
			end = p.Mark()
			p.Next()

		case Check:
			rv, err := v.Check(p)
			if err != nil {
				p.Goto(beg)
				return nil, err
			}
			end = rv
			p.Goto(rv)
			p.Next()

		case is.Opt:
			m, err := p.Expect(is.MinMax{v.This, 0, 1})
			if err != nil {
				p.Goto(beg)
				return nil, err
			}
			end = m

		case is.Not:
			if _, e := p.Check(v.This); e == nil {
				err := p.Expected(v)
				p.Goto(beg)
				return nil, err
			}
			end = p.Mark()

		case is.Min:
			c := 0
			last := p.Mark()
			var err error
			var m *Mark
			for {
				m, err = p.Expect(v.Match)
				if err != nil {
					break
				}
				last = m
				c++
			}
			if c < v.Min {
				p.Goto(beg)
				return nil, err
			}
			end = last

		case is.Count:
			m, err := p.Expect(is.MinMax{v.Match, v.Count, v.Count})
			if err != nil {
				p.Goto(beg)
				return nil, err
			}
			end = m

		case is.MinMax:
			c := 0
			last := p.Mark()
			var err error
			var m *Mark
			for {
				m, err = p.Expect(v.Match)
				if err != nil {
					break
				}
				last = m
				c++
			}
			if c == 0 && v.Min == 0 {
				if end == nil {
					end = last
				}
				continue
			}
			if !(v.Min <= c && c <= v.Max) {
				p.Goto(last)
				return nil, err
			}
			end = last

		case is.Seq:
			m, err := p.Expect(v...)
			if err != nil {
				p.Goto(beg)
				return nil, err
			}
			end = m

		case is.OneOf:
			var m *Mark
			var err error
			for _, i := range v {
				m, err = p.Expect(i)
				if err == nil {
					break
				}
			}
			if m == nil {
				return nil, p.Expected(v)
			}
			end = m

		default:
			return nil, fmt.Errorf("expect: unsupported argument type (%T)", m)
		}
	}
	return end, nil
}

func (p *Parser) Expected(this interface{}) error {
	var msg string
	but := fmt.Sprintf(` but got %v`, p)
	if p.Done() {
		runes := `runes`
		if p.mk.Pos.Rune == 1 {
			runes = `rune`
		}
		but = fmt.Sprintf(` but exceeded data length (%v %v)`, p.mk.Pos.Rune, runes)
	}
	// TODO add verbose errors for *all* types in Grammar
	switch v := this.(type) {
	case string:
		msg = fmt.Sprintf(`expected string %q`, v)
	case rune:
		msg = fmt.Sprintf(`expected rune %q`, v)
	case Class:
		msg = fmt.Sprintf(`expected class %v (%v)`, v.Ident(), v.Desc())
	default:
		msg = fmt.Sprintf(`expected %T %q`, v, v)
	}
	return errors.New(msg + but)
}

func (p *Parser) Check(ms ...interface{}) (*Mark, error) {
	defer p.Goto(p.Mark())
	return p.Expect(ms...)
}
