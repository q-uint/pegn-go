package pegn_test

import (
	"fmt"

	"gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
)

func ExampleNew() {
	p := new(pegn.Parser)
	p.Print()
	// Output:
	// <nil>
}

func ExampleParser_Init() {
	p := new(pegn.Parser)
	p.Init("something")
	p.Print()
	err := p.Init("")
	fmt.Println(err)
	p.Init("b")
	p.Print()
	p.Next()
	p.Print()
	fmt.Println(p.Done())
	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// parser: no input
	// U+0062 'b' 1,1-1 (1-1)
	// ENDOFDATA
	// true
}

func ExampleParser_Done_simple() {
	p := new(pegn.Parser)
	p.Init("Hi 👌 u.")
	p.Move(6)
	p.Print()
	p.Next()
	fmt.Println(p.Done())
	// Output:
	// U+002E '.' 1,7-10 (7-10)
	// true
}

func ExampleParser_Done_goto() {
	p := new(pegn.Parser)
	p.Init("Hi 👌 u.")
	m := p.Mark()
	p.Move(6)
	p.Print()
	p.Goto(m)
	p.Print()
	p.Move(6)
	p.Print()
	p.Next()
	fmt.Println(p.Done())
	// Output:
	// U+002E '.' 1,7-10 (7-10)
	// U+0048 'H' 1,1-1 (1-1)
	// U+002E '.' 1,7-10 (7-10)
	// true
}

func ExampleParser_Next() {

	p := new(pegn.Parser)
	s := "so\U0001F47Fmething here\nsomething there"
	fmt.Println(len(s))
	fmt.Println(len([]rune(s)))
	p.Init(s)
	p.Print()
	p.Next()
	p.Print()
	p.Next()
	p.Print()
	p.Next()
	p.Print()

	// Output:
	// 34
	// 31
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
	// U+1F47F '👿' 1,3-3 (3-3)
	// U+006D 'm' 1,4-7 (4-7)

}

func ExampleParser_Parse_first() {
	p := new(pegn.Parser)
	p.Init("something")
	p.Print()
	m := p.Mark()
	fmt.Println(p.Parse(m) == "s")
	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// true
}

func ExampleParser_Parse_emoji() {
	p := new(pegn.Parser)
	p.Init("Hi 👌 you.")
	p.Print()
	b := p.Mark()
	p.Next()
	p.Next()
	p.Next()
	p.Next()
	p.Next()
	p.Next()
	p.Next()
	p.Next()
	e := p.Mark()
	fmt.Println(p.Parse(b))
	e.Print()
	// Output:
	// U+0048 'H' 1,1-1 (1-1)
	// Hi 👌 you.
	// U+002E '.' 1,9-12 (9-12)
}

func ExampleParser_Parse_something() {

	p := new(pegn.Parser)
	p.Init("something")
	p.Print()
	m := p.Mark()
	p.Next()
	p.Next()
	p.Next()
	p.Print()
	fmt.Println(p.Parse(m))

	m2 := p.Mark()
	p.Goto(m)
	p.Print()
	fmt.Println(p.Parse(m2))

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+0065 'e' 1,4-4 (4-4)
	// some
	// U+0073 's' 1,1-1 (1-1)
	// some

}

func ExampleParser_Slice() {

	p := new(pegn.Parser)
	p.Init("something")
	p.Print()
	p.Move(3)
	p.Print()
	b := p.Mark()
	p.Move(2)
	p.Print()
	e := p.Mark()
	fmt.Println(p.Slice(b, e))

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+0065 'e' 1,4-4 (4-4)
	// U+0068 'h' 1,6-6 (6-6)
	// eth

}

func ExampleParser_Expect_rune() {
	var m *pegn.Mark

	p := new(pegn.Parser)
	p.Init("something")

	m, _ = p.Expect('s')
	m.Print()
	p.Print()

	m, _ = p.Expect('o')
	m.Print()
	p.Print()

	p.Move(2)
	p.Print()

	m, _ = p.Expect('t')
	m.Print()
	p.Print()

	p.Move(3)

	m, _ = p.Expect('g')
	m.Print()
	p.Print()

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
	// U+006F 'o' 1,2-2 (2-2)
	// U+006D 'm' 1,3-3 (3-3)
	// U+0074 't' 1,5-5 (5-5)
	// U+0074 't' 1,5-5 (5-5)
	// U+0068 'h' 1,6-6 (6-6)
	// U+0067 'g' 1,9-9 (9-9)
	// ENDOFDATA
}

func ExampleParser_Expect_string() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("something")
	m, _ = p.Expect("some")
	m.Print()
	m, _ = p.Expect("thing")
	m.Print()
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// U+0067 'g' 1,9-9 (9-9)
}

func ExampleParser_Expect_empty_string() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("some")
	m, err := p.Expect("")
	m.Print()
	fmt.Println(err)
	// Output:
	// <nil>
	// expect: cannot parse empty string

}

func ExampleParser_Expect_class() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("something")
	p.Move(3)
	m, err := p.Expect(is.Alpha)
	m.Print()
	fmt.Println(err)
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// <nil>
}

/*

func ExampleParser_Expect_checkfunc() {
	p := new(pegn.Parser)
	p.Init("something")
	p.Move(3)
	p.Print() // U+0065 'e' 1,4-4 (4-4)

	ck_e := func(p *pegn.Parser) (*pegn.Mark, error) {
		m := p.Mark()
		if m.Rune != 'e' {
			return nil, p.Expected('e')
		}
		return m, nil
	}

	ck_t := func(p *pegn.Parser) (*pegn.Mark, error) {
		return p.Check('t')
	}

	ck_hing := func(p *pegn.Parser) (*pegn.Mark, error) {
		return p.Check("hing")
	}

	var m *pegn.Mark

	m, _ = p.Expect(ck_e)
	m.Print() // U+0065 'e' 1,4-4 (4-4)
	p.Print() // U+0074 't' 1,5-5 (5-5)

	m, _ = p.Expect(ck_t)
	m.Print() // U+0074 't' 1,5-5 (5-5)

	m, _ = p.Expect(ck_hing)
	m.Print() // U+0067 'g' 1,9-9 (9-9)

	p.Init("     some")
	m, _ = p.Expect(is.Min{' ', 1})
	m.Print() // U+0020 ' ' 1,5-5 (5-5)
	p.Print()

	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// U+0065 'e' 1,4-4 (4-4)
	// U+0074 't' 1,5-5 (5-5)
	// U+0074 't' 1,5-5 (5-5)
	// U+0067 'g' 1,9-9 (9-9)
	// U+0020 ' ' 1,5-5 (5-5)
	// U+0073 's' 1,6-6 (6-6)
}
*/

func ExampleParser_Expect_not() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("something")
	m, _ = p.Expect(is.Not{'s'})
	m.Print()
	m, _ = p.Expect(is.Not{'z'})
	m.Print()
	p.Next()
	m, _ = p.Expect(is.Not{'0'})
	m.Print()
	// Output:
	// <nil>
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
}

func ExampleParser_Expect_count() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("####")
	m, _ = p.Expect(is.Count{'#', 4})
	m.Print()
	// Output:
	// U+0023 '#' 1,4-4 (4-4)
}

func ExampleParser_Expect_seq() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("####------")
	m, _ = p.Expect(is.Seq{is.Min{'#', 1}, is.Min{'-', 1}})
	m.Print()
	// Output:
	// U+002D '-' 1,10-10 (10-10)
}

func ExampleParser_Expect_oneof() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	set := is.OneOf{'-', 't', 's'}
	p.Init("s")
	m, _ = p.Expect(set)
	m.Print()
	p.Init("-")
	m, _ = p.Expect(set)
	m.Print()
	p.Init("t")
	m, _ = p.Expect(set)
	m.Print()
	p.Init("q")
	m, _ = p.Expect(set)
	m.Print()
	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+002D '-' 1,1-1 (1-1)
	// U+0074 't' 1,1-1 (1-1)
	// <nil>
}

func ExampleParser_Expect_compound() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("   U+002D")
	m, _ = p.Expect(is.Min{' ', 0}, "U+", is.MinMax{is.Upperhex, 4, 8})
	m.Print()
	p.Print()
	// Output:
	// U+0044 'D' 1,9-9 (9-9)
	// ENDOFDATA
}

func ExampleParser_Expect_compound_tokenid() {
	p := new(pegn.Parser)
	p.Init("TOKENID")
	m, err := p.Check(is.Upper, is.Min{is.OneOf{is.Upper, is.Seq{'_', is.Upper}}, 1})
	m.Print()
	p.Print()
	fmt.Println(err)
	// Output:
	// U+0044 'D' 1,7-7 (7-7)
	// U+0054 'T' 1,1-1 (1-1)
	// <nil>
}

func ExampleParser_Check() {
	var m *pegn.Mark

	p := new(pegn.Parser)

	p.Init("    something")
	p.Print() // U+0020 ' ' 1,1-1 (1-1)

	m, _ = p.Check(is.Min{' ', 1})
	m.Print() // U+0020 ' ' 1,4-4 (4-4)
	p.Print() // (didn't advance)

	fmt.Println(p.Check('\t'))
	p.Print() // (still not advanced)

	m, _ = p.Check(is.Min{'\t', 0})
	m.Print() // (true but not advanced)

	p.Init("-")
	p.Print()
	m, _ = p.Check('-')
	m.Print()

	// Output:
	// U+0020 ' ' 1,1-1 (1-1)
	// U+0020 ' ' 1,4-4 (4-4)
	// U+0020 ' ' 1,1-1 (1-1)
	// <nil> expected rune '\t' but got U+0020 ' ' 1,1-1 (1-1)
	// U+0020 ' ' 1,1-1 (1-1)
	// U+0020 ' ' 1,1-1 (1-1)
	// U+002D '-' 1,1-1 (1-1)
	// U+002D '-' 1,1-1 (1-1)
}

func ExampleParser_Check_emoji() {
	p := new(pegn.Parser)
	str := ""
	p.Init("Hi 👌 you.")
	for {
		m, err := p.Check(is.Unipoint)
		if err != nil {
			break
		}
		r := p.Parse(m)
		str += r
		p.Goto(m)
		p.Next()
	}
	fmt.Println(str)
	// Output:
	// Hi 👌 you.
}

func ExampleParser_Expect_min0max1_1() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("some")
	m, _ = p.Expect("some", is.MinMax{"!", 0, 2})
	m.Print()
	p.Print()
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// ENDOFDATA
}

func ExampleParser_Expect_min0max1_2() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("some!")
	m, _ = p.Expect("some", is.MinMax{"!", 0, 2})
	m.Print()
	p.Print()
	// Output:
	// U+0021 '!' 1,5-5 (5-5)
	// ENDOFDATA
}

func ExampleParser_Expect_min0max1_3() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("   something")
	m, _ = p.Expect(is.MinMax{' ', 2, 4}) // consume spaces
	m.Print()
	p.Print()
	m, _ = p.Expect(is.MinMax{'z', 0, 4})
	m.Print()
	p.Print()
	// Output:
	// U+0020 ' ' 1,3-3 (3-3)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
}

func ExampleParser_Expect_min() {
	var m *pegn.Mark
	p := new(pegn.Parser)
	p.Init("   something")
	m, _ = p.Expect(is.Min{' ', 2}) // consume spaces
	m.Print()
	p.Print()
	m, _ = p.Expect(is.Min{'z', 0})
	m.Print()
	p.Print()
	// Output:
	// U+0020 ' ' 1,3-3 (3-3)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
}

func ExampleParser_Expected() {
	p := new(pegn.Parser)
	p.Init("something")
	p.Next()
	fmt.Println(p.Expected("at least something"))
	// Output:
	// expected string "at least something" but got U+006F 'o' 1,2-2 (2-2)
}

func ExampleParser_Expect_expected_string() {
	p := new(pegn.Parser)
	p.Init("something")
	m, err := p.Expect("someone")
	m.Print()
	p.Print()
	fmt.Println(err)
	// Output:
	// <nil>
	// U+0073 's' 1,1-1 (1-1)
	// expected rune 'o' but got U+0074 't' 1,5-5 (5-5)
}

func ExampleParser_Expect_expected_rune() {
	p := new(pegn.Parser)
	p.Init("something")
	p.Move(3)
	p.Print()
	_, err := p.Expect('t') // on e
	fmt.Println(err)
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// expected rune 't' but got U+0065 'e' 1,4-4 (4-4)
}

func ExampleParser_Expect_expected_class() {
	p := new(pegn.Parser)
	p.Init("something")
	_, err := p.Expect(is.Digit)
	fmt.Println(err)
	// Output:
	// expected class digit (ASCII digit 0 through 9) but got U+0073 's' 1,1-1 (1-1)
}

func ExampleParser_Expect_class_upper() {
	p := new(pegn.Parser)
	p.Init("SOMETHING")
	n, err := p.Expect(is.Upper, is.Upper)
	n.Print()
	fmt.Println(err)
	// Output:
	// U+004F 'O' 1,2-2 (2-2)
	// <nil>
}

func ExampleParser_Expect_expected_class_upper() {
	p := new(pegn.Parser)
	p.Init("9SOMETHING")
	_, err := p.Expect(is.Upper)
	fmt.Println(err)
	// Output:
	// expected class upper (any ASCII uppercase letter) but got U+0039 '9' 1,1-1 (1-1)
}

func ExampleParser_Expect_expected_min() {
	p := new(pegn.Parser)
	p.Init("something")
	_, err := p.Expect(is.Min{"8", 1})
	fmt.Println(err)
	// Output:
	// expected rune '8' but got U+0073 's' 1,1-1 (1-1)
}

func ExampleParser_Expect_expected_minmax() {
	p := new(pegn.Parser)
	p.Init("------")
	_, err := p.Expect(is.MinMax{'-', 10, 14})
	fmt.Println(err)
	// Output:
	// expected rune '-' but exceeded data length (7 runes)
}

func ExampleParser_Expect_expected_count() {
	p := new(pegn.Parser)
	p.Init("-----")
	_, err := p.Expect(is.Count{'-', 12})
	fmt.Println(err)
	// Output:
	// expected rune '-' but exceeded data length (6 runes)
}

func ExampleParser_Expect_expected_seq() {
	p := new(pegn.Parser)
	p.Init("-----")
	_, err := p.Expect("---", is.Seq{'-', '*'})
	fmt.Println(err)
	// Output:
	// expected rune '*' but got U+002D '-' 1,5-5 (5-5)
}

func ExampleParser_Expect_expected_oneof() {
	p := new(pegn.Parser)
	p.Init("-----")
	_, err := p.Expect(is.OneOf{'!', '*'})
	fmt.Println(err)
	// Output:
	// expected is.OneOf ['!' '*'] but got U+002D '-' 1,1-1 (1-1)
}
