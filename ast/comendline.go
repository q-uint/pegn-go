package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// ComEndLine <- SP* ('# ' Comment)? EndLine
func ComEndLine(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ComEndLine, nd.NodeTypes)
	var n *pegn.Node
	var err error
	beg := p.Mark()

	// SP*
	p.Expect(is.Min{' ', 1})

	// ('# ' Comment)?
	for {

		b := p.Mark()

		// '# '
		_, err = p.Expect("# ")
		if err != nil {
			p.Goto(b)
			break
		}

		// Comment
		n, err = Comment(p)
		if err != nil {
			p.Goto(b)
			break
		}
		node.AppendChild(n)

		break
	}

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(beg)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
