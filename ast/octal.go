package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Octal <-- 'o' octdig+
func Octal(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Octal, nd.NodeTypes)

	var m *pegn.Mark

	// 'o' oct+
	m, _ = p.Check("o", is.Min{is.Octdig, 1})
	if m == nil {
		return expected("'o'", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
