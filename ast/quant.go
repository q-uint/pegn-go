package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Quant <- Optional / MinZero / MinOne / MinMax / Count
func Quant(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Quant, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Optional
	n, err = Optional(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// MinZero
	n, err = MinZero(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// MinOne
	n, err = MinOne(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// MinMax
	n, err = MinMax(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Count
	n, err = Count(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return expected("Optional / MinZero / MinOne / MinMax / Count", node, p)

}
