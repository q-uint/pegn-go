package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Lang <-- upper{2,12}
func Lang(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Lang, nd.NodeTypes)

	var m *pegn.Mark

	// upper{2,12}
	m, _ = p.Check(is.MinMax{is.Upper, 2, 12})
	if m == nil {
		return expected("upper{2,12}", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
