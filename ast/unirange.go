package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// UniRange <-- '[' Uni '-' Uni ']'
func UniRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.UniRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect("[")
	if err != nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Uni
	n, err = Unicode(p)
	if err != nil {
		p.Goto(beg)
		return expected("Uni", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect("-")
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Uni
	n, err = Unicode(p)
	if err != nil {
		p.Goto(beg)
		return expected("Uni", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect("]")
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
