package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Spacing <- ComEndLine? SP+
func Spacing(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Spacing, nd.NodeTypes)
	var n *pegn.Node
	var err error

	// ComEndLine?
	n, err = ComEndLine(p)
	if err == nil {
		node.AdoptFrom(n)
	}

	// SP+
	_, err = p.Expect(is.Min{' ', 1})
	if err != nil {
		return expected("SP+", node, p)
	}

	return node, nil
}
