package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Rule <- PosLook / NegLook / Plain
func Rule(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Rule, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// PosLook
	n, err = PosLook(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// NegLook
	n, err = NegLook(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Plain
	n, err = Plain(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return expected("PosLook / NegLook / Plain", node, p)
}
