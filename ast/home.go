package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

func Home(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Home, nd.NodeTypes)

	var m *pegn.Mark

	m, _ = p.Check(is.Min{is.Seq{is.Not{is.Ws}, is.Unipoint}, 1})
	if m == nil {
		return expected("(!ws unipoint)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
