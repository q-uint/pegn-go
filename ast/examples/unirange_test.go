package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleUniRange() {

	var n *pegn.Node

	p := new(pegn.Parser)

	p.Init("[u0000-u00FF]")
	n, _ = ast.UniRange(p)
	n.Print()

	p.Init("[u00FF-u1FFFF]")
	n, _ = ast.UniRange(p)
	n.Print()

	// Output:
	// ["UniRange", [
	//   ["Unicode", "u0000"],
	//   ["Unicode", "u00FF"]
	// ]]
	// ["UniRange", [
	//   ["Unicode", "u00FF"],
	//   ["Unicode", "u1FFFF"]
	// ]]

}
