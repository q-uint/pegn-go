package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMeta() {
	var n *pegn.Node

	// Meta <- "# " Language " (" Version ") " Home EndLine
	p := new(pegn.Parser)

	// # PEGN (v0.31.1) gitlab.com/pegn/spec
	p.Init("# PEGN (v0.31.1-alpha) gitlab.com/pegn/spec\n")
	n, _ = ast.Meta(p)
	n.Print()

	// Output:
	// ["Meta", [
	//   ["Lang", "PEGN"],
	//   ["MajorVer", "0"],
	//   ["MinorVer", "31"],
	//   ["PatchVer", "1"],
	//   ["PreVer", "alpha"],
	//   ["Home", "gitlab.com/pegn/spec"],
	//   ["EndLine", "\n"]
	// ]]

}
