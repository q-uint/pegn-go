package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleCopyright() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("# Copyright 2020 Robert S Muhlestein (rwx@robs.io)\n")
	n, _ = ast.Copyright(p)
	n.Print()
	// Output:
	// ["Copyright", [
	//   ["Comment", "2020 Robert S Muhlestein (rwx@robs.io)"],
	//   ["EndLine", "\n"]
	// ]]
}
