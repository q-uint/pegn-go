package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleOctRange() {
	var n *pegn.Node

	// OctRange <-- "[" Octal "-" Octal "]"
	p := new(pegn.Parser)

	// [o0-o7]
	p.Init("[o0-o7]")
	n, _ = ast.OctRange(p)
	n.Print()

	// [o00-o77]
	p.Init("[o00-o77]")
	n, _ = ast.OctRange(p)
	n.Print()

	// Output:
	// ["OctRange", [
	//   ["Octal", "o0"],
	//   ["Octal", "o7"]
	// ]]
	// ["OctRange", [
	//   ["Octal", "o00"],
	//   ["Octal", "o77"]
	// ]]

}
