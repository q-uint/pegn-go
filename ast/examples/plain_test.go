package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExamplePlain() {
	var n *pegn.Node
	p := new(pegn.Parser)

	p.Init("ThisIsPlain")
	n, _ = ast.Plain(p)
	n.Print()

	p.Init("class")
	n, _ = ast.Plain(p)
	n.Print()

	p.Init("TOKEN")
	n, _ = ast.Plain(p)
	n.Print()

	// Output:
	// ["Plain", [
	//   ["CheckId", "ThisIsPlain"]
	// ]]
	// ["Plain", [
	//   ["ClassId", "class"]
	// ]]
	// ["Plain", [
	//   ["TokenId", "TOKEN"]
	// ]]

}
