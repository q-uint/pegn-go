package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMax() {
	var n *pegn.Node

	// Max <-- digit+
	p := new(pegn.Parser)

	// 1
	p.Init("1")
	n, _ = ast.Max(p)
	n.Print()

	// 99
	p.Init("99")
	n, _ = ast.Max(p)
	n.Print()

	// Output:
	// ["Max", "1"]
	// ["Max", "99"]

}
