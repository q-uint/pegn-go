package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleBinary() {

	var n *pegn.Node

	// Binary <-- "b" bitdig+
	p := new(pegn.Parser)

	// b01
	p.Init("b01")
	n, _ = ast.Binary(p)
	n.Print()

	// b01010110
	p.Init("b01010110")
	n, _ = ast.Binary(p)
	n.Print()

	// Output:
	// ["Binary", "b01"]
	// ["Binary", "b01010110"]

}
