package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExamplePrimary_token() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("TK")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["TokenId", "TK"]
	// ]]
}

func ExamplePrimary_checkid() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("MyCheckId")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["CheckId", "MyCheckId"]
	// ]]
}

func ExamplePrimary_expression() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("(Some Expr Here?)")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Some"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Expr"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Here"],
	//         ["Optional", "?"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExamplePrimary_string() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("<--")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// <nil>
}
