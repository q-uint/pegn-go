package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleRange() {
	var n *pegn.Node

	// Range <- AlphaRange / IntRange / UniRange
	//        / BinRange / HexRange / OctRange
	p := new(pegn.Parser)

	// [u0000-u00FF]
	p.Init("[u0000-u00FF]")
	n, _ = ast.UniRange(p)
	n.Print()

	// [a-z]
	p.Init("[a-z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// [00-99]
	p.Init("[00-99]")
	n, _ = ast.IntRange(p)
	n.Print()

	// [b01-b10]
	p.Init("[b01-b10]")
	n, _ = ast.BinRange(p)
	n.Print()

	// [x0-xF]
	p.Init("[x0-xF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// Output:
	// ["UniRange", [
	//   ["Unicode", "u0000"],
	//   ["Unicode", "u00FF"]
	// ]]
	// ["AlphaRange", [
	//   ["Letter", "a"],
	//   ["Letter", "z"]
	// ]]
	// ["IntRange", [
	//   ["Integer", "00"],
	//   ["Integer", "99"]
	// ]]
	// ["BinRange", [
	//   ["Binary", "b01"],
	//   ["Binary", "b10"]
	// ]]
	// ["HexRange", [
	//   ["Hexadec", "x0"],
	//   ["Hexadec", "xF"]
	// ]]

}
