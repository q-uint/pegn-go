package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleHexRange() {
	var n *pegn.Node

	// HexRange <-- "[" Hexadec "-" Hexadec "]"
	p := new(pegn.Parser)

	// [x0-xF]
	p.Init("[x0-xF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// [x00-xFF]
	p.Init("[x00-xFF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// Output:
	// ["HexRange", [
	//   ["Hexadec", "x0"],
	//   ["Hexadec", "xF"]
	// ]]
	// ["HexRange", [
	//   ["Hexadec", "x00"],
	//   ["Hexadec", "xFF"]
	// ]]

}
