package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleUnicode() {

	var n *pegn.Node

	p := new(pegn.Parser)

	p.Init("u0000")
	n, _ = ast.Unicode(p)
	n.Print()

	p.Init("uffffff")
	n, _ = ast.Unicode(p)
	n.Print()

	p.Init("uFFFFF")
	n, _ = ast.Unicode(p)
	n.Print()

	p.Init("uFFFFFF")
	n, _ = ast.Unicode(p)
	n.Print()

	// Output:
	// ["Unicode", "u0000"]
	// <nil>
	// ["Unicode", "uFFFFF"]
	// <nil>

}
