package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMinMax() {
	var n *pegn.Node

	// MinMax <-- "{" Min "," Max? "}"
	p := new(pegn.Parser)

	// {1,}
	p.Init("{1,}")
	n, _ = ast.MinMax(p)
	n.Print()

	// {0,9}
	p.Init("{0,9}")
	n, _ = ast.MinMax(p)
	n.Print()

	// Output:
	// ["MinMax", [
	//   ["Min", "1"]
	// ]]
	// ["MinMax", [
	//   ["Min", "0"],
	//   ["Max", "9"]
	// ]]

}
