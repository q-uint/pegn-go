package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleLetter() {

	var n *pegn.Node

	// Letter <-- alpha
	p := new(pegn.Parser)

	// a
	p.Init("a")
	n, _ = ast.Letter(p)
	n.Print()

	// A
	p.Init("A")
	n, _ = ast.Letter(p)
	n.Print()

	// z
	p.Init("z")
	n, _ = ast.Letter(p)
	n.Print()

	// Z
	p.Init("Z")
	n, _ = ast.Letter(p)
	n.Print()

	// _
	p.Init("_")
	n, _ = ast.Letter(p)
	n.Print()

	// Output:
	// ["Letter", "a"]
	// ["Letter", "A"]
	// ["Letter", "z"]
	// ["Letter", "Z"]
	// <nil>

}
