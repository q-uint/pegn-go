package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleLang() {
	var n *pegn.Node

	// Lang <-- upper{2,12}
	p := new(pegn.Parser)

	// PEGN
	p.Init("PEGN")
	n, _ = ast.Lang(p)
	n.Print()

	// Output:
	// ["Lang", "PEGN"]

}
