package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// OctRange <-- '[' Octal '-' Octal ']'
func OctRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.OctRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect('[')
	if err != nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Octal
	n, err = Octal(p)
	if err != nil {
		p.Goto(beg)
		return expected("Octal", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect('-')
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Octal
	n, err = Octal(p)
	if err != nil {
		p.Goto(beg)
		return expected("Octal", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect(']')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
