package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Letter <-- alpha
func Letter(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Letter, nd.NodeTypes)

	var err error
	var m *pegn.Mark

	// alpha
	m, err = p.Check(is.Alpha)
	if err != nil {
		return expected("alpha", node, p)
	}

	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
