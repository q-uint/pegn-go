package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// MajorVer <-- digit+
func MajorVer(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.MajorVer, nd.NodeTypes)

	var m *pegn.Mark

	// digit+
	m, _ = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return expected("digit+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
