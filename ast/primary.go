package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Primary <- Simple / CheckId / '(' Expression ')'
func Primary(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Primary, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Primary
	n, err = Simple(p)
	if err == nil {
		node.AdoptFrom(n)
		return node, nil
	}

	// CheckId
	n, err = CheckId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}
	b := p.Mark()

	// '('
	_, err = p.Expect('(')
	if err != nil {
		p.Goto(b)
		return expected("'('", node, p)
	}

	// Expression
	n, err = Expression(p)
	if err != nil {
		p.Goto(b)
		return expected("Expression", node, p)
	}

	// ')'
	_, err = p.Expect(')')
	if err != nil {
		p.Goto(b)
		return expected("')'", node, p)
	}

	node.AppendChild(n)

	return node, nil
}
