package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// ResClassId <-- 'alphanum' / 'alpha' / 'any' / 'bindig' / 'control'
//              / 'digit' / 'hexdig' / 'lowerhex' / 'lower' / 'octdig'
//              / 'punct' / 'quotable' / 'sign' / 'uphex' / 'upper'
//              / 'visible' / 'ws' / 'alnum' / 'ascii' / 'blank' / 'cntrl'
//              / 'graph' / 'print' / 'space' / 'word' / 'xdigit'
//              / 'unipoint'
func ResClassId(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ResClassId, nd.NodeTypes)
	var m *pegn.Mark

	for _, v := range []string{
		"alphanum", "alpha", "any", "bindig", "control", "digit", "hexdig",
		"lowerhex", "lower", "octdig", "punct", "quotable", "sign",
		"uphex", "upper", "visible", "ws", "alnum", "ascii", "blank",
		"cntrl", "graph", "print", "space", "word", "xdigit", "unipoint",
	} {
		m, _ = p.Check(v)
		if m == nil {
			continue
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

		return node, nil
	}

	return expected("'alphanum' / 'alpha' / 'any' / 'bindig' / 'control' / 'digit' / 'hexdig' / 'lowerhex' / 'lower' / 'octdig' / 'punct' / 'quotable' / 'sign' / 'upperhex' / 'upper' / 'visible' / 'ws' / 'alnum' / 'ascii' / 'blank' / 'cntrl' / 'graph' / 'print' / 'space' / 'word' / 'xdigit'", node, p)
}
