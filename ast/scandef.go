package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// ScanDef <-- CheckId SP+ '<-' SP+ Expression
func ScanDef(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ScanDef, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// CheckId
	n, err = CheckId(p)
	if err != nil {
		p.Goto(beg)
		return expected("CheckId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	_, err = p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1})
	if err != nil {
		p.Goto(beg)
		return expected("SP+ '<-' SP+", node, p)
	}

	// Expression
	n, err = Expression(p)
	if n == nil {
		p.Goto(beg)
		return expected("Expression", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
