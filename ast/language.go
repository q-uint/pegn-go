package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Language <- Lang ('-' LangExt)?
func Language(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Language, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Lang
	n, err = Lang(p)
	if err != nil {
		return expected("Lang", node, p)
	}
	node.AppendChild(n)

	for {

		// '-'
		_, err = p.Expect('-')
		if err != nil {
			break
		}

		// LangExt?
		n, err = LangExt(p)
		if err != nil {
			break
		}
		node.AppendChild(n)

		break
	}

	return node, nil
}
