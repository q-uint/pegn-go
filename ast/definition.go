package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Definition <- SchemaDef / ScanDef / ClassDef / TokenDef
func Definition(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Definition, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// NodeDef
	n, err = NodeDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}
	// ScanDef
	n, err = ScanDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// ClassDef
	n, err = ClassDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// TokenDef
	n, err = TokenDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return nil, err
}
