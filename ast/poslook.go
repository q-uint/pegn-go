package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// PosLook <-- '&' Primary Quant?
func PosLook(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.PosLook, nd.NodeTypes)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '&'
	_, err = p.Expect('&')
	if err != nil {
		p.Goto(b)
		return expected("'%'", node, p)
	}

	// Primary
	n, err = Primary(p)
	if err != nil {
		p.Goto(b)
		return expected("Primary", node, p)
	}
	node.AdoptFrom(n)

	// Quant?
	n, err = Quant(p)
	if err == nil {
		node.AdoptFrom(n)
	}

	return node, nil
}
